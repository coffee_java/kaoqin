package com.coffee.kaoqin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class ProcessDatFile {
	public static List<File> processFile(File file) throws Exception {
		FileReader fr=new FileReader(file);
	    BufferedReader br=new BufferedReader(fr);
	    String str = null;
	    boolean isStart=false;
	    File output=null;
	    BufferedWriter writer=null;
        while((str = br.readLine()) != null){
            if(StringUtils.contains(str,"PROP RPM")) {
                String fileName="";
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(str);
                while(m.find()) {
                    fileName=m.group();
                }
                output=new File(file.getParentFile()+File.separator+"RPM"+fileName+".dat");
                writer=new BufferedWriter(new FileWriter(output));
                continue;
            }
            if(StringUtils.contains(str,"(mph)")) {
                isStart=true;
                continue;
            }
            if(isStart) {
                if(StringUtils.isBlank(str)) {
                    isStart=false;
                    writer.flush();
                    writer.close();
                    continue;
                }
                writer.write(str);
                writer.newLine();
            }
            
        }
        return null;
	}
}
