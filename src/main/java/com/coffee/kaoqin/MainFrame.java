package com.coffee.kaoqin;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

public class MainFrame {
	private static String version="v1.0";
	private static  JFrame frame;
	private static  JButton jButton;

	public static void main(String[] args) {
		MainFrame.showFrame();
	}

	private static void showFrame(){
		frame = new JFrame();
		jButton = new javax.swing.JButton();

		frame.setBounds(400, 400, 320, 120);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		frame.setTitle("考勤分析 "+version);
		frame.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
//        frame.setIconImage(getImagePath("logo.png"));
		frame.setMaximumSize(new java.awt.Dimension(320, 120));
		frame.setMinimumSize(new java.awt.Dimension(320, 120));
		frame.setName("考勤分析 "+version);

		jButton.setText("选择文件");
		jButton.setPreferredSize(new java.awt.Dimension(100, 60));
		jButton.setToolTipText("");

		jButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				JFileChooser jfc=new JFileChooser();
				jfc.setFileFilter(new FileFilter() {

					@Override
					public String getDescription() {
						return "只能选excel文件";
					}

					@Override
					public boolean accept(File f) {
						String fileName=f.getName();
						return (fileName.endsWith(".xls"));
					}
				});
				jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jfc.showDialog(new JLabel(), "选择文件");
				File file=jfc.getSelectedFile();
				if (file==null) {
					return;
				}

				if (JOptionPane.showConfirmDialog(null, "确定处理文件："+file.getAbsolutePath()+"?") == 0) {
					try {
						File output=ImportExcel.processFile(file);
						JOptionPane.showMessageDialog(null, "文件保存到："+output.getAbsolutePath(), "处理文件成功", JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "失败原因："+e.getMessage(), "处理文件失败", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
				}
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(frame.getContentPane());
		frame.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addContainerGap(60, Short.MAX_VALUE)
								.addComponent(jButton)
								.addGap(49, 60, 200)
						)
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addGap(10, 20, 45)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(jButton)
								)
								.addContainerGap(20, Short.MAX_VALUE))
		);
		frame.setVisible(true);
	}
}
